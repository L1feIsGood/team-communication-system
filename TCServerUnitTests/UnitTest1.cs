﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TCServer;

namespace TCServerUnitTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void SayTest()
        {
            ChatService chatService = new ChatService();
            chatService.Say(new Group("MyTestGroup"), 
                new Message("TestMessage", DateTime.Now.Date.ToLongDateString(),
                                            DateTime.Now.ToLongTimeString()));
        }

        [TestMethod]
        public void LoadHistoryTest()
        {
            ChatService chatService = new ChatService();
            var t = chatService.LoadHistory(new Group("MyTestGroup"));
        }

        [TestMethod]
        public void LoginTest()
        {
            ChatService chatService = new ChatService();
            chatService.LogIn(new User("TestUser"));
        }
        [TestMethod]
        public void LogoutTest()
        {
            ChatService chatService = new ChatService();
            chatService.LogOut(new User("TestUser"));
        }
        [TestMethod]
        public void CreateGroupTest()
        {
            ChatService chatService = new ChatService();
            //chatService.CreateGroup(new Group("TestGroup1"),new User("TestUser1"));
        }
        [TestMethod]
        public void AddGroupMemberTest()
        {
            ChatService chatService = new ChatService();
            chatService.AddGroupMember(new User("TestUser2"), new Group("MyTestGroup"));
        }
        [TestMethod]
        public void LeaveGroup()
        {
            ChatService chatService = new ChatService();
            chatService.LeaveGroup(new User("TestUser2"), new Group("MyTestGroup"));
        }
        [TestMethod]
        public void GetUsersOnlineTest()
        {
            ChatService chatService = new ChatService();
            var t = chatService.GetUsersOnline();
        }
        [TestMethod]
        public void GetUsersInGroupTest()
        {
            ChatService chatService = new ChatService();
            var t = chatService.GetUsersInGroup(new Group("Custom"));
        }
    }
}
