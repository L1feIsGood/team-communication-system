﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Runtime.InteropServices;
    
namespace TCServer
{
    internal class Program
    {
        private static ServiceHost chatHost;
        private static void Main(string[] args)
        {

            chatHost = new ServiceHost(typeof (ChatService));

            chatHost.Opened += host_Opened;
            chatHost.Closed += host_Closed;
            chatHost.Open();

            Console.ReadLine();
            chatHost.Close();
        }

        private static void host_Closed(object sender, EventArgs e)
        {
            Console.WriteLine("Server is closed. ");
        }

        private static void host_Opened(object sender, EventArgs e)
        {
            Console.WriteLine("Server is running..." + chatHost.BaseAddresses.First());
        }
    }
}
