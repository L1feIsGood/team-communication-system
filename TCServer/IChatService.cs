﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace TCServer
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract(CallbackContract = typeof (IChatServiceCallback))]
    public interface IChatService
    {
        /// <summary>
        /// Tests the connection
        /// </summary>
        /// <returns>any return value is good (default is true)</returns>
        [OperationContract]
        bool TestConnection();

        /// <summary>
        /// Send message to all users in group
        /// </summary>
        /// <param name="group">current group (only name is needed)</param>
        /// <param name="sender">sender (only NickName is needed)</param>
        /// <param name="message">sending message (date, time and text)</param>
        [OperationContract(IsOneWay = true)]
        void Say(Group group, Message message);

        /// <summary>
        /// Load list of messages of group
        /// </summary>
        /// <param name="group">needed group</param>
        /// <returns>List of History instances</returns>
        [OperationContract]
        List<History> LoadHistory(Group group);

        /// <summary>
        /// Add user in the root of XML database and in the callback list
        /// </summary>
        /// <param name="user"></param>
        /// <returns>result of authorization</returns>
        [OperationContract]
        bool LogIn(User user);

        /// <summary>
        /// Remove user from the root of XML database and from the callback list
        /// </summary>
        /// <param name="user"></param>
        [OperationContract]
        void LogOut(User user);

        /// <summary>
        /// Add group to database XMl and add creator into it
        /// </summary>
        /// <param name="group">new group (only name is needed)</param>
        /// <param name="creator">creator (only NickName is needed)</param>
        [OperationContract]
        void CreateGroup(Group group);

        /// <summary>
        /// Add user to group
        /// </summary>
        /// <param name="member">new member (only NickName is needed)</param>
        /// <param name="group">existing group (only Name is needed)</param>
        [OperationContract]
        void AddGroupMember(User member, Group group);

        /// <summary>
        /// Remove user from group
        /// </summary>
        /// <param name="leaver">leaver (only NickName is needed)</param>
        /// <param name="group">some group (only Name is needed)</param>
        [OperationContract]
        void LeaveGroup(User leaver, Group group);
        /// <summary>
        /// Get list of users from root of XML (online)
        /// </summary>
        /// <returns>list of users(online)</returns>
        [OperationContract]
        List<User> GetUsersOnline();
        /// <summary>
        /// Get list of users which are in group
        /// </summary>
        /// <param name="group">existing group</param>
        /// <returns>list of users(in group)</returns>
        [OperationContract]
        List<User> GetUsersInGroup(Group group);
    }
    /// <summary>
    /// interface which should be implemented on the client side
    /// </summary>
    [ServiceContract]
    public interface IChatServiceCallback
    {
        /// <summary>
        /// Receives message from sender
        /// </summary>
        /// <param name="message">received message</param>
        /// <param name="user">sender</param>
        [OperationContract(IsOneWay = true)]
        void ReceiveMessage(Message message, User user);
    }
}
