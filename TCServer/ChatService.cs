﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mime;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Security.Policy;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using System.Xml.Linq;

namespace TCServer
{
    //[ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Reentrant)]
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerSession)]
    [DataContract(IsReference = true)]
    public partial class ChatService : IChatService
    {
        private static string DataBasePath = AppDomain.CurrentDomain.BaseDirectory + "\\DataBase.xml";
        private XDocument DataBaseDoc = XDocument.Load(DataBasePath);

        private static Dictionary<XElement, IChatServiceCallback> clientCallbacks =
            new Dictionary<XElement, IChatServiceCallback>();

        private IChatServiceCallback _currentCallback;

        private User currentUser = new User("Anonymous");


        public bool TestConnection()
        {
            return true;
        }


        public void Say(Group group, Message message)
        {
            var currentRoot = DataBaseDoc.Root;
            //look for group
            var currentGroup =
                currentRoot?.Descendants(nameof(Group))
                    .First(node => node.Attribute(nameof(Group.Name)).Value == group.Name);
            //look for user
            var currentUser =
                currentGroup?.Descendants(nameof(User))
                    .First(node => node.Attribute(nameof(User.NickName)).Value== this.currentUser.NickName);
            //build Xmessage
            var mess = new XElement(nameof(Message),
                new XAttribute(nameof(Message.Text), message.Text),
                new XAttribute(nameof(Message.Date), message.Date),
                new XAttribute(nameof(Message.Time), message.Time));
            //memorize currentUser Xelement
            XElement currentUserCopy = new XElement(currentUser);
            currentUserCopy.RemoveNodes();
            
            //Compare without nodes
            var usersInCurrentGroup = clientCallbacks.Where(p=>p.Key.ToString()==currentUserCopy.ToString());
            //send message to users in group
            foreach (var x in usersInCurrentGroup)
            {
                x.Value.ReceiveMessage(message, this.currentUser);
            }
            //add Xmessage
            currentUser?.Add(mess);
            DataBaseDoc.Save(DataBasePath);

        }

        public List<History> LoadHistory(Group group)
        {
            var currentRoot = DataBaseDoc.Root;
            //look for group
            var currentGroup =
                currentRoot?.Descendants(nameof(Group))
                    .First(node => node.Attribute(nameof(Group.Name)).Value == group.Name);
            //look for users
            var users = currentGroup?.Descendants(nameof(User)).ToArray();
            var usersArr = users?.Select(u =>
                new User( (string) u.Attribute(nameof(User.NickName)) ?? String.Empty)).ToArray();
            var result = new List<History>();
            //add History
            for (int i = 0; i < users?.Length; i++)
            {
                var messeges = users[i].Descendants(nameof(Message)).Select(m => new Message(
                    (string) m.Attribute(nameof(Message.Text)) ?? String.Empty,
                    (string) m.Attribute(nameof(Message.Date)) ?? String.Empty,
                    (string) m.Attribute(nameof(Message.Time)) ?? String.Empty)).ToArray();
                foreach (Message t in messeges)
                {
                    result.Add(new History(
                        new User(usersArr[i].NickName),
                        new Message(t.Text, t.Date, t.Time)));
                }
            }
            return result;
        }

        public bool LogIn(User user)
        {
            if (OperationContext.Current != null)
            {
                if (user.NickName == null)
                {
                    string windowsName = OperationContext.Current.ServiceSecurityContext.PrimaryIdentity.Name;
                    //AddNewBee(windowsName.Substring(windowsName.IndexOf("\\", StringComparison.Ordinal) + 1));
                    AddNewBee(windowsName);
                }
                else
                    AddNewBee(user.NickName);
            }
            else return false;
            currentUser = user;
            return true;
        }

        private void AddNewBee(string name)
        {
            var newBee = new XElement(nameof(User), new XAttribute(nameof(User.NickName), name));
            DataBaseDoc.Root?.Add(newBee);
            DataBaseDoc.Save(DataBasePath);
            if (OperationContext.Current != null)
            {
                _currentCallback = OperationContext.Current.GetCallbackChannel<IChatServiceCallback>();
                clientCallbacks.Add(newBee, _currentCallback);
            }
        }

        public void LogOut(User user)
        {
            var xgroup = DataBaseDoc.Root?.Elements(nameof(User));
            var xuser = xgroup?.First(us => us.Attribute(nameof(User.NickName)).Value == user.NickName);
            if (xuser != null)
            {
                xuser.Remove();
                DataBaseDoc.Save(DataBasePath);
                clientCallbacks.Remove(xuser);
            }
        }

        public void CreateGroup(Group group)
        {
            DataBaseDoc.Root?.Add(new XElement(nameof(Group),new XAttribute(nameof(Group.Name),group.Name),
                new XElement(nameof(User), new XAttribute(nameof(User.NickName), currentUser.NickName))));
            DataBaseDoc.Save(DataBasePath);
        }

        public void AddGroupMember(User member, Group group)
        {
            var xgroup = DataBaseDoc.Root?.Elements(nameof(Group))
                .First(gr=> gr.Attribute(nameof(Group.Name)).Value==group.Name);
            xgroup.Add(new XElement(nameof(User), new XAttribute(nameof(User.NickName), member.NickName)));
            DataBaseDoc.Save(DataBasePath);
        }

        public void EnterGroup(User user, Group group)
        {
            //то же самое что и AddGroupMember
            throw new NotImplementedException();
        }

        public void LeaveGroup(User leaver, Group group)
        {
            var xgroup = DataBaseDoc.Root?.Elements(nameof(Group))
    .First(gr => gr.Attribute(nameof(Group.Name)).Value == group.Name);
            xgroup?.Elements().First(us => us.Attribute(nameof(User.NickName)).Value == leaver.NickName).Remove();
            DataBaseDoc.Save(DataBasePath);
        }

        public List<User> GetUsersOnline()
        {
            return
                DataBaseDoc.Root?.Elements(nameof(User))
                    .Select(u => new User(u.Attribute(nameof(User.NickName)).Value))
                    .ToList();
        }

        public List<User> GetUsersInGroup(Group group)
        {
            return DataBaseDoc.Root?.Elements(nameof(Group))
                .First(gr => gr.Attribute(nameof(Group.Name)).Value == group.Name).Elements(nameof(User))
                .Select(u => new User(u.Attribute(nameof(User.NickName)).Value)).ToList();
        }
    }
}
