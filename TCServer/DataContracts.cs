﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace TCServer
{
    [DataContract]
    public class Group
    {
        public Group(string name)
        {
            Name = name;
        }

        [DataMember] public string Creator;
        [DataMember] public string Name;
    }


    [DataContract]
    public class User
    {
        public User(string nickName)
        {
            NickName = nickName;
        }

        [DataMember] public string NickName;
        [DataMember] public string Password;
        [DataMember] public string Email;

        public override string ToString()
        {
            return NickName;
        }
    }

    [DataContract]
    public class Message
    {
        public Message(string text, string date, string time)
        {
            Text = text;
            Date = date;
            Time = time;
        }

        [DataMember] public string Text;
        [DataMember] public string Date;
        [DataMember] public string Time;
    }

    [DataContract]
    public class History
    {
        public History(User user, Message message)
        {
            User = user;
            Message = message;
        }

        [DataMember] private User User;
        [DataMember] private Message Message;
    }
}

