﻿namespace TeamCommunicationSystem.ChatService
{
    public partial class User
    {
        public override string ToString()
        {
            return this.NickName;
        }
    }
    public  partial class Message
    {
        public override string ToString()
        {
            return $"{this.Date} \r\n {this.Text}\r\n ";
        }
    }
}