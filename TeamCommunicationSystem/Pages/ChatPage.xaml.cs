﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using TeamCommunicationSystem.ChatService;
using TeamCommunicationSystem.Windows;

namespace TeamCommunicationSystem.Pages
{
    /// <summary>
    /// Interaction logic for ChatPage.xaml
    /// </summary>
    public partial class ChatPage : UserControl, IChatServiceCallback
    {
        private Group group;
        private ChatTabPage selectedChatPage;
        public event EventHandler AddChat;

        public ChatPage()
        {
            InitializeComponent();
            DispatcherTimer timer = new DispatcherTimer(TimeSpan.FromSeconds(3), DispatcherPriority.Background,
                UpdateMembers, Dispatcher.CurrentDispatcher);
            timer.Start();
            //for (int i = 0; i < 5; i++)
            //{
            //    var ctp = new ChatTabPage();
            //    var tb = new TabItem
            //    {
            //        Header = "Chat" + (i+1),
            //        Content = ctp
            //    };
            //    ChatTab.Items.Add(tb);
            //}

        }

        private async void UpdateMembers(object sender, EventArgs e)
        {
            var selectedTabItem = ChatTab.SelectedItem as TabItem;
            selectedChatPage = selectedTabItem?.Content as ChatTabPage;
            if (group != null)
            {
                User[] users = await Core.Client.GetUsersInGroupAsync(group);
                selectedChatPage?.LbChatMembers.Items.Clear();
                foreach (var user in users)
                {
                    selectedChatPage?.LbChatMembers.Items.Add(user);
                }
            }
        }

        public void AddChatTab(Group group)
        {
            var ctp = new ChatTabPage(group);
            var tb = new TabItem
            {
                Header = group.Name,
                Content = ctp
            };
            this.group = group;
            ChatTab.Items.Add(tb);
            
            ChatTab.SelectedItem = tb;
            
        }

        private void BtnLeave_Click(object sender, RoutedEventArgs e)
        {
            ChatTab.Items.RemoveAt(ChatTab.SelectedIndex);
        }

        private void BtnAdd_Click(object sender, RoutedEventArgs e)
        {
            AddChat?.Invoke(this, EventArgs.Empty);
        }

        private void ChatTab_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdateMembers(this, EventArgs.Empty);
        }

        public void ReceiveMessage(Message message, User user)
        {
            selectedChatPage.LbChat.Items.Add(message);
        }
    }
}
