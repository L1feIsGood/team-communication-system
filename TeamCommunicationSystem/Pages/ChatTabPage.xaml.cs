﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TeamCommunicationSystem.ChatService;

namespace TeamCommunicationSystem.Pages
{
    /// <summary>
    /// Interaction logic for ChatTabPage.xaml
    /// </summary>
    public partial class ChatTabPage : UserControl
    {
        private Group group; 
        public event EventHandler EditChatClick;
        public ChatTabPage(Group group)
        {
            InitializeComponent();
            this.group = group;
        }

        private void TbText_OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                //TODO: Server request to send message
                Message msg = new Message
                {
                    Text = TbText.Text,
                    Date = DateTime.Now.ToString(CultureInfo.CurrentCulture),
                    Time = DateTime.Now.ToString(CultureInfo.CurrentCulture)
                };

                Core.Client.Say(group, msg);
                TbText.Text = String.Empty;
            }
        }

        private void BtnEditChat_Click(object sender, RoutedEventArgs e)
        {
            EditChatClick?.Invoke(this, EventArgs.Empty);
        }
    }
}
