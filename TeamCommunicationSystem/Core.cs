﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using TeamCommunicationSystem.ChatService;
using TeamCommunicationSystem.Enums;
using TeamCommunicationSystem.Pages;
using TeamCommunicationSystem.Utilities;
using TeamCommunicationSystem.Windows;

namespace TeamCommunicationSystem
{
    public static class Core
    {
        public static ChatPage chatPage;
        public static MainWindow MainWindow { get; set; }
        public static bool Initialized { get; private set; }

        public static ChatServiceClient Client { get; set; }
        public static User User { get; set; }

        public static void Initialize()
        {
            Directory.SetCurrentDirectory(AppDomain.CurrentDomain.BaseDirectory);
            var newUser = !Directory.Exists(Config.AppDataPath);
            Config.Load();
            Logger.Initialize();
            Helper.UpdateAppTheme();
            var splashScreenWindow = new SplashScreenWindow();
            splashScreenWindow.ShowConditional();
            Thread.Sleep(800);
            LoginType loginType;
            LoginWindow loginWindow = null;
            var loggedIn = LoggingApi.LoadCredentials();
            if (!loggedIn && Config.Instance.ShowLoginDialog)
            {
                chatPage = new ChatPage();
                var inst = new InstanceContext(chatPage);
                //inst.Host.
                Uri uri = new Uri("http://tempuri.org/");
                Client = new ChatServiceClient(inst, "WSDualHttpBinding_IChatService");
                Client.ClientCredentials.Windows.ClientCredential =
                    CredentialCache.DefaultNetworkCredentials.GetCredential(uri, "Basic");

                loginWindow = new LoginWindow(Client);
                splashScreenWindow.Hide();
                loginWindow.ShowDialog();
                if (loginWindow.LoginResult == LoginType.None)
                {
                    Application.Current.Shutdown();
                    return;
                }
                loginType = loginWindow.LoginResult;
                splashScreenWindow = new SplashScreenWindow();
                splashScreenWindow.ShowConditional();
            }
            else
                loginType = loggedIn ? LoginType.AutoLogin : LoginType.AutoGuest;
            User = loginWindow?.User;
            MainWindow = new MainWindow(Client);
            //MainWindow.LoadConfigSettings();
            MainWindow.Show();
            splashScreenWindow.Hide();
            
            //BackupManager.Run();

            //UpdateOverlayAsync();
            Initialized = true;

        }
    }
}
