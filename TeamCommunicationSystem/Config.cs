﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using TeamCommunicationSystem.Utilities;

namespace TeamCommunicationSystem
{
    public class Config
    {
        #region Settings

        private static Config _config;

        public static readonly string AppDataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)
                                             + @"\TeamCommunicationSystem";

        [DefaultValue("Blue")]
        public string AccentName = "Blue";

        [DefaultValue(true)]
        public bool AdditionalOverlayTooltips = true;

        [DefaultValue(true)]
        public bool AlwaysOverwriteLogConfig = true;

        [DefaultValue(".")]
        public string DataDirPath = ".";

        [DefaultValue(-1)]
        public int IgnoreNewsId = -1;

        [DefaultValue(0L)]
        public long LastHearthStatsDecksSync = 0L;

        [DefaultValue(0L)]
        public long LastHearthStatsGamesSync = 0L;

        [DefaultValue(false)]
        public bool LogConfigConsolePrinting = false;

        [DefaultValue(0)]
        public int LogLevel = 0;

        [DefaultValue(false)]
        public bool MinimizeToTray = false;

        [DefaultValue(false)]
        public bool NoteDialogDelayed = false;

        [DefaultValue(0)]
        public int OffsetX = 0;

        [DefaultValue(0)]
        public int OffsetY = 0;

        [DefaultValue(true)]
        public bool RememberHearthStatsLogin = true;

        [DefaultValue(false)]
        public bool ResolvedDeckStatsIds = false;

        [DefaultValue(false)]
        public bool ResolvedDeckStatsIssue = false;

        [DefaultValue("Theme")]
        public string SelectedWindowBackground = "Theme";

        [DefaultValue("c7b1c7904951f7a")]
        public string ImgurClientId = "c7b1c7904951f7a";

        [DefaultValue(true)]
        public bool? SaveConfigInAppData = null;
        
        [DefaultValue(true)]
        public bool? SaveDataInAppData = null;

        [DefaultValue(true)]
        public bool SaveInAppData = true;

        [DefaultValue(false)]
        public bool ShowInTaskbar = false;

        [DefaultValue(true)]
        public bool ShowLoginDialog = true;

        [DefaultValue(true)]
        public bool ShowSplashScreen = true;

        [DefaultValue(false)]
        public bool ShowLogTab = false;

        [DefaultValue(false)]
        public bool ShowNoteDialogAfterGame = false;

        [DefaultValue(false)]
        public bool SortDecksByClassArena = false;

        [DefaultValue(false)]
        public bool StartMinimized = false;

        [DefaultValue(false)]
        public bool StartWithWindows = false;

        [DefaultValue(false)]
        public bool StatsInWindow = false;

        [DefaultValue("Blue")]
        public string ThemeName = "Blue";

        [DefaultValue(620)]
        public int WindowHeight = 620;

        [DefaultValue(550)]
        public int WindowWidth = 550;

        [DefaultValue("#696969")]
        public string WindowsBackgroundHex = "#696969";

        [DefaultValue(false)]
        public bool WindowsTopmost = false;

        [DefaultValue(false)]
        public bool WindowsTopmostIfHsForeground = false;
        #endregion

        #region Properties

        [Obsolete]
        public string HomeDir
        {
            get { return Instance.SaveInAppData ? AppDataPath + "/" : string.Empty; }
        }

        public string BackupDir
        {
            get { return Path.Combine(DataDir, "Backups"); }
        }

        public string ConfigPath => Instance.ConfigDir + "config.xml";

        public string HearthStatsFilePath => Path.Combine(DataDir, "hearthstatsauth");

        public string ConfigDir => Instance.SaveConfigInAppData == false ? string.Empty : AppDataPath + "\\";

        public string DataDir => Instance.SaveDataInAppData == false ? DataDirPath + "\\" : AppDataPath + "\\";
       
        public static Config Instance
        {
            get
            {
                if (_config == null)
                {
                    _config = new Config();
                    _config.ResetAll();
                }

                return _config;
            }
        }

        #endregion

        #region Misc

        private Config()
        {
        }

        public static void Save()
        {
            XmlManager<Config>.Save(Instance.ConfigPath, Instance);
        }

        public static void SaveBackup(bool deleteOriginal = false)
        {
            var configPath = Instance.ConfigPath;

            if (File.Exists(configPath))
            {
                File.Copy(configPath, configPath + DateTime.Now.ToFileTime());

                if (deleteOriginal)
                    File.Delete(configPath);
            }
        }

        public static void Load()
        {
            var foundConfig = false;
            Environment.CurrentDirectory = AppDomain.CurrentDomain.BaseDirectory;
            try
            {
                if (File.Exists("config.xml"))
                {
                    _config = XmlManager<Config>.Load("config.xml");
                    foundConfig = true;
                }
                else if (File.Exists(AppDataPath + @"\config.xml"))
                {
                    _config = XmlManager<Config>.Load(AppDataPath + @"\config.xml");
                    foundConfig = true;
                }
                else if (!Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)))
                    //save locally if appdata doesn't exist (when e.g. not on C)
                    Instance.SaveConfigInAppData = false;
            }
            catch (Exception e)
            {
                MessageBox.Show(
                                e.Message + "\n\n" + e.InnerException + "\n\n If you don't know how to fix this, please delete "
                                + Instance.ConfigPath, "Error loading config.xml");
                Application.Current.Shutdown();
            }

            if (!foundConfig)
            {
                if (Instance.ConfigDir != string.Empty)
                    Directory.CreateDirectory(Instance.ConfigDir);
                Save();
            }
            else if (Instance.SaveConfigInAppData != null)
            {
                if (Instance.SaveConfigInAppData.Value) //check if config needs to be moved
                {
                    if (File.Exists("config.xml"))
                    {
                        Directory.CreateDirectory(Instance.ConfigDir);
                        SaveBackup(true); //backup in case the file already exists
                        File.Move("config.xml", Instance.ConfigPath);
                        Logger.WriteLine("Moved config to appdata", "Config");
                    }
                }
                else if (File.Exists(AppDataPath + @"\config.xml"))
                {
                    SaveBackup(true); //backup in case the file already exists
                    File.Move(AppDataPath + @"\config.xml", Instance.ConfigPath);
                    Logger.WriteLine("Moved config to local", "Config");
                }
            }
        }

        public void ResetAll()
        {
            foreach (var field in GetType().GetFields())
            {
                var attr = (DefaultValueAttribute)field.GetCustomAttributes(typeof(DefaultValueAttribute), false).FirstOrDefault();
                if (attr != null)
                    field.SetValue(this, attr.Value);
            }
        }

        public void Reset(string name)
        {
            var proper = GetType().GetFields().First(x => x.Name == name);
            var attr = (DefaultValueAttribute)proper.GetCustomAttributes(typeof(DefaultValueAttribute), false).First();
            proper.SetValue(this, attr.Value);
        }

        [AttributeUsage(AttributeTargets.All, Inherited = false)]
        private sealed class DefaultValueAttribute : Attribute
        {
            // This is a positional argument
            public DefaultValueAttribute(object value)
            {
                Value = value;
            }

            public object Value { get; private set; }
        }

        #endregion
    }
}
