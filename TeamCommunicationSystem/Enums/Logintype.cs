﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamCommunicationSystem.Enums
{
    public enum LoginType
    {
        None,
        AutoLogin,
        AutoGuest,
        Login,
        Guest,
        Register
    }
}
