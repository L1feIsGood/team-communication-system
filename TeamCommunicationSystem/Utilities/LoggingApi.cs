﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TeamCommunicationSystem.Utilities
{
    public class LoggingApi
    {
        private static string _baseUrl = "http://api.hearthstats.net/api/v3";

        private static string BaseUrl
        {
            get { return _baseUrl; }
            set { _baseUrl = value; }
        }

        #region authentication

        private static string _authToken;

        public static bool IsLoggedIn => !string.IsNullOrEmpty(_authToken);

        public static bool Logout()
        {
            Logger.WriteLine("Logged out.", "HearthStatsAPI");
            _authToken = "";
            try
            {
                if (File.Exists(Config.Instance.HearthStatsFilePath))
                    File.Delete(Config.Instance.HearthStatsFilePath);
                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteLine("Error deleting hearthstats credentials file\n" + ex, "HearthStatsAPI");
                return false;
            }
        }

        public static string LoggedInAs { get; private set; }

        public static bool LoadCredentials()
        {
            if (File.Exists(Config.Instance.HearthStatsFilePath))
            {
                try
                {
                    Logger.WriteLine("Loading stored credentials...", "HearthStatsAPI");
                    using (var reader = new StreamReader(Config.Instance.HearthStatsFilePath))
                    {
                        dynamic content = JsonConvert.DeserializeObject(reader.ReadToEnd());
                        _authToken = content.auth_token;
                        LoggedInAs = content.email;
                    }
                    return true;
                }
                catch (Exception e)
                {
                    Logger.WriteLine("Error loading credentials\n" + e, "HearthStatsAPI");
                    return false;
                }
            }
            return false;
        }

        public static async Task<LoginResult> LoginAsync(string email, string password)
        {
            try
            {
                Logger.WriteLine("Logging in...", "HearthStatsAPI");
                var url = BaseUrl + "/users/sign_in";
                var data = JsonConvert.SerializeObject(new {user_login = new {email, password}});
                var json = await PostAsync(url, Encoding.UTF8.GetBytes(data));
                dynamic response = JsonConvert.DeserializeObject(json);
                if ((bool) response.success)
                {
                    Logger.WriteLine("Successfully logged in.", "HearthStatsAPI");
                    _authToken = response.auth_token;
                    LoggedInAs = response.email;
                    if (Config.Instance.RememberHearthStatsLogin)
                    {
                        using (var writer = new StreamWriter(Config.Instance.HearthStatsFilePath, false))
                            writer.Write(JsonConvert.SerializeObject(new {auth_token = _authToken, email}));
                    }
                    return new LoginResult(true);
                }
                Logger.WriteLine("Error logging in...", "HearthStatsAPI");
                return new LoginResult(false, response.ToString());
            }
            catch (Exception e)
            {
                Logger.WriteLine(e.ToString(), "HearthStatsAPI");
                return new LoginResult(false, e.Message);
            }
        }


        public static async Task<LoginResult> RegisterAsync(string email, string password)
        {
            try
            {
                var url = BaseUrl + "/users";
                var data = JsonConvert.SerializeObject(new {user = new {email, password}});
                var json = await PostAsync(url, Encoding.UTF8.GetBytes(data));
                dynamic response = JsonConvert.DeserializeObject(json);
                if ((string) response.email == email && (int) response.id > 0)
                    return new LoginResult(true);
                return new LoginResult(false, response.ToString());
            }
            catch (Exception e)
            {
                Logger.WriteLine(e.ToString(), "HearthStatsAPI");
                return new LoginResult(false, e.Message);
            }
        }

        #endregion

        #region webrequests

        private static async Task<string> PostAsync(string url, string data)
        {
#if DEBUG
            Logger.WriteLine("> " + data, "HearthStatsAPI");
#endif
            return await PostAsync(url, Encoding.UTF8.GetBytes(data));
        }

        private static async Task<string> PostAsync(string url, byte[] data)
        {
            try
            {
                var request = CreateRequest(url, "POST");
                using (var stream = await request.GetRequestStreamAsync())
                    stream.Write(data, 0, data.Length);
                var webResponse = await request.GetResponseAsync();
                using (var responseStream = webResponse.GetResponseStream())
                using (var reader = new StreamReader(responseStream))
                {
                    var response = reader.ReadToEnd();
#if DEBUG
                    Logger.WriteLine("< " + response, "HearthStatsAPI");
#endif
                    return response;
                }
            }
            catch (WebException e)
            {
                throw;
            }
        }

        private static HttpWebRequest CreateRequest(string url, string method)
        {
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.ContentType = "application/json";
            request.Accept = "application/json";
            request.Method = method;
            return request;
        }

        #endregion
    }
}
