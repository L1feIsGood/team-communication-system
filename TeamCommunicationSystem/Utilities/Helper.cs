﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using MahApps.Metro;
using MediaColor = System.Windows.Media.Color;

namespace TeamCommunicationSystem.Utilities
{
    public static class Helper
    {
        private static Version _currentVersion;
        public static Version GetCurrentVersion()
        {
            try
            {
                return _currentVersion ?? (_currentVersion = new Version(XmlManager<SerializableVersion>.Load("Version.xml").ToString()));
            }
            catch (Exception e)
            {
                MessageBox.Show(
                                e.Message + "\n\n" + e.InnerException
                                + "\n\n If you don't know how to fix this, please overwrite Version.xml with the default file.",
                                "Error loading Version.xml");

                return null;
            }
        }

        public static void UpdateAppTheme()
        {
            var theme = ThemeManager.DetectAppStyle().Item1;
            //This do not work, there is no blue theme in ThemeManager
            //var theme = string.IsNullOrEmpty(Config.Instance.ThemeName)
            //    ? ThemeManager.DetectAppStyle().Item1
            //    : ThemeManager.AppThemes.First(t => t.Name == Config.Instance.ThemeName);
            var accent = string.IsNullOrEmpty(Config.Instance.AccentName)
                ? ThemeManager.DetectAppStyle().Item2
                : ThemeManager.Accents.First(a => a.Name == Config.Instance.AccentName);
            ThemeManager.ChangeAppStyle(Application.Current, accent, theme);
            Application.Current.Resources["GrayTextColorBrush"] = theme.Name == "BaseLight"
                ? new SolidColorBrush((MediaColor) Application.Current.Resources["GrayTextColor1"])
                : new SolidColorBrush((MediaColor) Application.Current.Resources["GrayTextColor2"]);

        }

        public static long ToUnixTime(this DateTime time)
        {
            var total = (long)(time.ToUniversalTime() - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds;
            return total < 0 ? 0 : total;
        }
    }
}
