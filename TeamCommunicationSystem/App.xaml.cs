﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace TeamCommunicationSystem
{
    /// <summary>d
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void App_OnStartup(object sender, StartupEventArgs e)
        {
            Core.Initialize();
        }

        private void BtnClose_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Clicked!" + e.OriginalSource + e.Source);
        }

        private void BtnAddChat_Click(object sender, RoutedEventArgs e)
        {
            
        }
    }
}
