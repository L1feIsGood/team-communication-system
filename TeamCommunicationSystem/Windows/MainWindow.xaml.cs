﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.TextFormatting;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using TeamCommunicationSystem.ChatService;
using TeamCommunicationSystem.Pages;
using TeamCommunicationSystem.Utilities;

namespace TeamCommunicationSystem.Windows
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        private EmailsPage emailsPage;
        private SharedDocumentsPage sharedDocumentsPage;
        private ChatServiceClient client;
        public MainWindow()
        {
            InitializeComponent();
            Core.chatPage.AddChat += ChatPage_AddChat;
            emailsPage = new EmailsPage();
            sharedDocumentsPage = new SharedDocumentsPage();

            MainContent.Content = Core.chatPage;
            BtnChats.IsEnabled = false;


        }

        public MainWindow(ChatServiceClient client) : this()
        {
            this.client = client;
        }

        private async void ChatPage_AddChat(object sender, EventArgs e)
        {
            var result = await
                this.ShowMessageAsync("Adding new chat", "You can either enter existing chat or create a new one...",
                    MessageDialogStyle.AffirmativeAndNegative,
                    new MetroDialogSettings {AffirmativeButtonText = "enter", NegativeButtonText = "create"});

            if (result == MessageDialogResult.Affirmative)
            {
                // Enter button clicked
                var chatName = await this.ShowInputAsync("Entering chat", "Chat name:",
                    new MetroDialogSettings {AffirmativeButtonText = "ok", NegativeButtonText = "cancel"});

                if (!string.IsNullOrEmpty(chatName))
                {
                    //TODO: Server request to find chat by chatName
                    var group = new Group();
                    group.Name = chatName;
                    client.AddGroupMember(Core.User, group);

                    Core.chatPage.AddChatTab(group);

                    bool isPasswordRequired = true;
                    if (isPasswordRequired)
                    {
                        var chatPassword = await this.ShowInputAsync("Entering chat", "Chat password:",
                            new MetroDialogSettings {AffirmativeButtonText = "ok", NegativeButtonText = "cancel"});

                        //TODO: Server request to check password
                        bool isPasswordCorrect = true;
                        //if(isPasswordCorrect)
                            //chatPage.AddChatTab(chatName);
                    }
                    else
                    {
                       // chatPage.AddChatTab(chatName);
                    }

                }
            }
            else if (result == MessageDialogResult.Negative)
            {
                //Create button clicked
                var chatName = await this.ShowInputAsync("Creating chat: Step 1 of 2", "Enter chat name:",
                    new MetroDialogSettings { AffirmativeButtonText = "next", NegativeButtonText = "cancel" });

                if (!string.IsNullOrEmpty(chatName))
                {
                    var chatPassword = await this.ShowInputAsync("Creating chat: Step 2 of 2", "Enter password (if needed):",
                       new MetroDialogSettings { AffirmativeButtonText = "done", NegativeButtonText = "cancel" });

                    var group = new Group();
                    group.Name = chatName;
                    //group.Creator = ;
                    //TODO: Server request to create new chat
                    client.CreateGroup(group);

                    Core.chatPage.AddChatTab(group);
                }
            }
        }

        private void BtnOptions_OnClick(object sender, RoutedEventArgs e)
        {

        }

        private void BtnChats_OnClick(object sender, RoutedEventArgs e)
        {
            MainContent.Content = Core.chatPage;
            BtnChats.IsEnabled = false;
            BtnEmails.IsEnabled = true;
            BtnDocuments.IsEnabled = true;
        }

        private void BtnEmails_OnClick(object sender, RoutedEventArgs e)
        {
            MainContent.Content = emailsPage;
            BtnChats.IsEnabled = true;
            BtnEmails.IsEnabled = false;
            BtnDocuments.IsEnabled = true;
        }

        private void BtnDocuments_OnClick(object sender, RoutedEventArgs e)
        {
            MainContent.Content = sharedDocumentsPage;
            BtnChats.IsEnabled = true;
            BtnEmails.IsEnabled = true;
            BtnDocuments.IsEnabled = false;
        }

        private void BtnExit_OnClick(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
            Close();
        }
        private void MainWindow_OnClosing(object sender, CancelEventArgs cancelEventArgs)
        {
            Environment.Exit(0);
            Close();
        }
    }
}
