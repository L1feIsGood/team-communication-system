﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls.Dialogs;
using TeamCommunicationSystem.ChatService;
using TeamCommunicationSystem.Enums;
using TeamCommunicationSystem.Pages;
using TeamCommunicationSystem.Utilities;

namespace TeamCommunicationSystem.Windows
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : INotifyPropertyChanged
    {
        private readonly bool _initialized;
        private ProgressDialogController _controller;
        private Visibility _loginRegisterVisibility;
        private LoginType _loginResult = LoginType.None;
        private ChatServiceClient client;
        public User User;

        public LoginType LoginResult
        {
            get { return _loginResult; }
            private set { _loginResult = value; }
        }

        public LoginWindow(ChatServiceClient chatService)
        {
            InitializeComponent();
            client = chatService;
            CheckBoxRememberLogin.IsChecked = Config.Instance.RememberHearthStatsLogin;
            _initialized = true;
        }

        public double TabWidth
        {
            get { return ActualWidth/2; }
        }

        public Visibility LoginRegisterVisibility
        {
            get { return _loginRegisterVisibility; }
            set
            {
                _loginRegisterVisibility = value;
                OnPropertyChanged();
                OnPropertyChanged("ContinueAsGuestVisibility");
            }
        }

        public Visibility ContinueAsGuestVisibility
            => LoginRegisterVisibility == Visibility.Visible ? Visibility.Collapsed : Visibility.Visible;

        public event PropertyChangedEventHandler PropertyChanged;

        private void Hyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            Process.Start(e.Uri.AbsoluteUri);
        }

        private async void BtnLogin_Click(object sender, RoutedEventArgs e)
        {
            var email = TextBoxEmail.Text;
            if (string.IsNullOrEmpty(email) || !Regex.IsMatch(email, @".*@.*\..*"))
            {
                DisplayLoginError("Please enter an valid email address");
                return;
            }
            if (string.IsNullOrEmpty(TextBoxPassword.Password))
            {
                DisplayLoginError("Please enter a password");
                return;
            }
            IsEnabled = false;
            _controller = await this.ShowProgressAsync("Logging in...", "");


            //TODO: result = результат входа в систему (объект класса пользователь)
            //var inst = new InstanceContext(typeof (ChatTabPage));

            //client.ClientCredentials.Windows.ClientCredential = new NetworkCredential("Bob", "Likes", "Bobs");
            try
            {


                //client.Open();
                User user = new User
                {
                    NickName = email,
                    Password = TextBoxPassword.Password
                };
                User = user;
                var result = client.LogIn(user);
                //var result = await LoggingApi.LoginAsync(TextBoxEmail.Text, TextBoxPassword.Password);



                TextBoxPassword.Clear();
                if (result)
                {
                    LoginResult = LoginType.Login;
                    Close();
                }
                else
                    DisplayLoginError("Invalid email or password");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private async void DisplayLoginError(string error)
        {
            TextBlockErrorMessage.Text = error;
            TextBlockErrorMessage.Visibility = Visibility.Visible;
            IsEnabled = true;
            if (_controller != null)
            {
                if (_controller.IsOpen)
                {
                    await _controller.CloseAsync();
                }
            }

        }

        private void CheckBoxRememberLogin_Checked(object sender, RoutedEventArgs e)
        {
            if (!_initialized)
                return;
            Config.Instance.RememberHearthStatsLogin = true;
            Config.Save();
        }

        private void CheckBoxRememberLogin_OnUnchecked(object sender, RoutedEventArgs e)
        {
            if (!_initialized)
                return;
            Config.Instance.RememberHearthStatsLogin = false;
            Config.Save();
            try
            {
                if (File.Exists(Config.Instance.HearthStatsFilePath))
                    File.Delete(Config.Instance.HearthStatsFilePath);
            }
            catch (Exception ex)
            {
                Logger.WriteLine("Error deleting hearthstats credentials file\n" + ex, "HearthStatsAPI");
            }
        }

        private async void BtnRegister_Click(object sender, RoutedEventArgs e)
        {
            if (!CheckBoxPrivacyPolicy.IsChecked == true)
                return;

            var email = TextBoxRegisterEmail.Text;
            if (string.IsNullOrEmpty(email) || !Regex.IsMatch(email, @".*@.*\..*"))
            {
                DisplayLoginError("Please enter an valid email address");
                return;
            }
            if (string.IsNullOrEmpty(TextBoxRegisterPassword.Password))
            {
                DisplayLoginError("Please enter a password");
                return;
            }
            if (TextBoxRegisterPassword.Password.Length < 6)
            {
                DisplayLoginError("Your password needs to be at least 6 characters");
                return;
            }
            if (string.IsNullOrEmpty(TextBoxRegisterPasswordConfirm.Password))
            {
                DisplayLoginError("Please confirm your password");
                return;
            }
            if (!TextBoxRegisterPassword.Password.Equals(TextBoxRegisterPasswordConfirm.Password))
            {
                DisplayLoginError("Entered passwords do not match");
                return;
            }
            IsEnabled = false;
            _controller = await this.ShowProgressAsync("Registering account...", "");


            //TODO: result = результат регистрации (объект класса пользователь)
            //client.

            var result = await LoggingApi.RegisterAsync(email, TextBoxRegisterPassword.Password);


            if (result.Success)
            {
                _controller.SetTitle("Logging in...");
                result = await LoggingApi.LoginAsync(email, TextBoxRegisterPassword.Password);
            }
            else if (result.Message.Contains("422"))
                DisplayLoginError("Email already registered");
            else
                DisplayLoginError(result.Message);
            TextBoxRegisterPassword.Clear();
            TextBoxRegisterPasswordConfirm.Clear();
            if (result.Success)
            {
                LoginResult = LoginType.Register;
                Close();
            }
        }

        private void CheckBoxPrivacyPolicy_Checked(object sender, RoutedEventArgs e)
        {
            BtnRegister.IsEnabled = true;
        }

        private void CheckBoxPrivacyPolicy_OnUnchecked(object sender, RoutedEventArgs e)
        {
            BtnRegister.IsEnabled = false;
        }

        private void Button_Continue(object sender, RoutedEventArgs e)
        {
            LoginRegisterVisibility = Visibility.Collapsed;
            TabControlLoginRegister.SelectedIndex = 2;
        }

        private void ButtonBack_OnClick(object sender, RoutedEventArgs e)
        {
            LoginRegisterVisibility = Visibility.Visible;
            TabControlLoginRegister.SelectedIndex = 1;
        }

        private void Button_ContinueAnyway(object sender, RoutedEventArgs e)
        {
            Logger.WriteLine("Continuing as guest...");
            LoginResult = LoginType.Guest;
            Close();
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            handler?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}
