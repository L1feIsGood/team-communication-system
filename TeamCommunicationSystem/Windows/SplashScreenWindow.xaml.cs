﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TeamCommunicationSystem.Utilities;

namespace TeamCommunicationSystem.Windows
{
    /// <summary>
    /// Interaction logic for SplashScreenWindow.xaml
    /// </summary>
    public partial class SplashScreenWindow
    {
        public string VersionString { get; private set; }

        public SplashScreenWindow()
        {
            var version = Helper.GetCurrentVersion();
            VersionString = $"v{version.Major}.{version.Minor}.{version.Build}";
            InitializeComponent();
        }

        public void ShowConditional()
        {
            if (Config.Instance.ShowSplashScreen)
                this.Show();
        }
    }
}
